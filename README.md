[English](https://gitee.com/lbpeter0911/ajax-ts/blob/master/README.en.md) | 简体中文

## xfe-ajax-ts
所有逻辑来源与[xfe-ajax](https://gitee.com/xiejiahao/ajax)，本仓库去掉其他功能保留核心请求功能，并用ts重新构建。

## 使用
克隆项目并运行`npm run dev`,就可以查看示例了。根据示例来学习怎么使用吧。